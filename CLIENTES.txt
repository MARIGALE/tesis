using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IdentityModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CONTROL.CAPAPRESENTACION
{
    public partial class CLIENTES : Form
    {

        SqlConnection Conexion = new SqlConnection("Data Source =DESKTOP-5MDNRLG\\SQLEXPRESS;Initial Catalog=CRM_GALEANO ;Integrated Security=true");



        int CLIENTEID = 0;
      



        public CLIENTES()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BTN_GUARDAR_CLIENTES_Click(object sender, EventArgs e)
        {

           
                Conexion.Open();

            SqlCommand comando = new SqlCommand("INSERT_CLIENTE", Conexion);

            SqlTransaction transaction;
            transaction = Conexion.BeginTransaction(IsolationLevel.ReadCommitted);
            comando.Transaction = transaction;




            try


            {

                comando.CommandType = System.Data.CommandType.StoredProcedure;

                comando.Parameters.Add("@DPI", SqlDbType.Decimal).Value = Convert.ToInt64(TXT_DPI_CLIENTE.Text);
                comando.Parameters.Add("@NOMBRE1", SqlDbType.NVarChar).Value = TXT_NOMBRE1_CLIENTE.Text;
                comando.Parameters.Add("@NOMBRE2", SqlDbType.NVarChar).Value = TXT_NOMBRE2_CLIENTE.Text;
                comando.Parameters.Add("@Apellido1", SqlDbType.NVarChar).Value = TXT_APELLIDO1_CLIENTE.Text;
                comando.Parameters.Add("@Apellido2", SqlDbType.NVarChar).Value = TXT_APELLIDO2_CLIENTE.Text;

                comando.Parameters.Add("@FECHA", SqlDbType.Date).Value = TXT_FECHANACI_CLIENTE.Text;
                comando.Parameters.Add("@GENERO", SqlDbType.Int).Value = Convert.ToUInt32(CBX_GENERO.SelectedValue);
                comando.Parameters.Add("@DIRECCION", SqlDbType.NVarChar).Value = TXT_DIRECCION_CLIENTE.Text;
                comando.Parameters.Add("@TELEFONO", SqlDbType.NVarChar).Value = TXT_TELEFONO_CLIENTE.Text;
                comando.Parameters.Add("@CORREO", SqlDbType.NVarChar).Value = TXT_CORREO_CLIENTE.Text;

                comando.ExecuteNonQuery();



                transaction.Commit();
                MessageBox.Show("CLIENTE INGRESADO");
                Clear();
                LISTARCLIENTES();

                Conexion.Close();
            }

            catch (Exception)

            {
                transaction.Rollback();
                MessageBox.Show("OCURRIO UN FALLO" + e);
                Clear();

                Conexion.Close();




























                }

            }

            void Clear()
        {
            TXT_DPI_CLIENTE.Text = "";
            TXT_APELLIDO1_CLIENTE.Text = " ";
            TXT_NOMBRE1_CLIENTE.Text = "";
            TXT_NOMBRE2_CLIENTE.Text = "";
            TXT_APELLIDO1_CLIENTE.Text = "";
            TXT_APELLIDO2_CLIENTE.Text = "";
            TXT_FECHANACI_CLIENTE.Text = "";
            TXT_DIRECCION_CLIENTE.Text = "";
            TXT_TELEFONO_CLIENTE.Text = "";
            TXT_CORREO_CLIENTE.Text = "";



            CLIENTEID = 0;
         
            //BTN_ELIMINAR_CLIENTE.Enabled = false;

        }





        private void CBX_GENERO_SelectedIndexChanged(object sender, EventArgs e)
        {
           


        }




        public void LISTARGENERO()
        {
            //hacemos la consulta a la base de datos
            SqlCommand comando = new SqlCommand("SELECT_GENERO", Conexion);

            SqlDataAdapter adapter = new SqlDataAdapter();


            comando.CommandType = CommandType.StoredProcedure;


            SqlDataAdapter da = new SqlDataAdapter(comando);
            DataTable dt = new DataTable();

            da.Fill(dt);

            CBX_GENERO.ValueMember = "ID_GENERO";
            CBX_GENERO.DisplayMember = "GENERO";

            CBX_GENERO.DataSource = dt;

        }

        public void LISTARCLIENTES()
        {
            //hacemos la consulta a la base de datos
            SqlCommand comando = new SqlCommand("SELECT_CLIENTES", Conexion);

            comando.CommandType = System.Data.CommandType.StoredProcedure;
          

            SqlDataAdapter adaptador = new SqlDataAdapter();
            adaptador.SelectCommand = comando;
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            DGT_CLIENTE.DataSource = tabla;

            Conexion.Close();





        }


        private void CLIENTES_Load(object sender, EventArgs e)
        {
            LISTARGENERO();
            LISTARCLIENTES();


            }

        private void BTN_MOSTRAR_Click(object sender, EventArgs e)
        {
            //hacemos la consulta a la base de datos
            SqlCommand comando = new SqlCommand("SELECT_CLIENTES", Conexion);

            comando.CommandType = System.Data.CommandType.StoredProcedure;


            SqlDataAdapter adaptador = new SqlDataAdapter();
            adaptador.SelectCommand = comando;
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            DGT_CLIENTE.DataSource = tabla;



            Conexion.Close();
        }
        


       
       

        private void ELIMINAR_CLIENTE_Click(object sender, EventArgs e)
        {

            Conexion.Open();
            {
                
                SqlCommand Comando = new SqlCommand("ELIMINAR_CLIENTE", Conexion);
                Comando.CommandType = CommandType.StoredProcedure;
                Comando.Parameters.AddWithValue("@ID", CLIENTEID);
                Comando.ExecuteNonQuery();
                MessageBox.Show("CLIENTE ELIMINADO");
                Clear();
            }

        }
        private void DGT_CLIENTE_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (DGT_CLIENTE.CurrentRow.Index != -1)
            {

                try


                {



                    //DPI= Convert.ToInt64( DGT_CLIENTE.CurrentRow.Cells[1].Value.ToString());
                    TXT_DPI_CLIENTE.Text = DGT_CLIENTE.CurrentRow.Cells[1].Value.ToString();
                TXT_NOMBRE1_CLIENTE.Text = DGT_CLIENTE.CurrentRow.Cells[2].Value.ToString();
                TXT_NOMBRE2_CLIENTE.Text = DGT_CLIENTE.CurrentRow.Cells[3].Value.ToString();
                TXT_APELLIDO1_CLIENTE.Text = DGT_CLIENTE.CurrentRow.Cells[4].Value.ToString();
                TXT_APELLIDO2_CLIENTE.Text = DGT_CLIENTE.CurrentRow.Cells[5].Value.ToString();
                TXT_FECHANACI_CLIENTE.Text = DGT_CLIENTE.CurrentRow.Cells[6].Value.ToString();
                CBX_GENERO.Text = DGT_CLIENTE.CurrentRow.Cells[7].Value.ToString();
                TXT_DIRECCION_CLIENTE.Text = DGT_CLIENTE.CurrentRow.Cells[8].Value.ToString();
                TXT_TELEFONO_CLIENTE.Text = DGT_CLIENTE.CurrentRow.Cells[9].Value.ToString();
                TXT_CORREO_CLIENTE.Text = DGT_CLIENTE.CurrentRow.Cells[10].Value.ToString();


                CLIENTEID = Convert.ToInt32(DGT_CLIENTE.CurrentRow.Cells[0].Value.ToString());


                //BTN_ACTUALIZAR_CLIENTE.Text = "ACTUALIZAR";
                //BTN_ELIMINAR_CLIENTE.Enabled = Enabled;



                }

                catch (Exception)

                {
                   
                    MessageBox.Show("SELECCIONE LA LINEA");
                    Clear();

                    Conexion.Close();
                }
            }
        }
        

        private void BTN_ACTUALIZAR_CLIENTE_Click(object sender, EventArgs e)
        {

            
                Conexion.Open();

                SqlCommand comando = new SqlCommand("ACTUALIZAR_CLIENTE", Conexion);

            SqlTransaction transaction;
            transaction = Conexion.BeginTransaction(IsolationLevel.ReadCommitted);
            comando.Transaction = transaction;




            try


            {

                comando.CommandType = System.Data.CommandType.StoredProcedure;

                    comando.Parameters.AddWithValue("@id", CLIENTEID);
                    comando.Parameters.Add("@DPI", SqlDbType.Decimal).Value = Convert.ToInt64(TXT_DPI_CLIENTE.Text);
                    comando.Parameters.Add("@NOMBRE1", SqlDbType.NVarChar).Value = TXT_NOMBRE1_CLIENTE.Text;
                    comando.Parameters.Add("@NOMBRE2", SqlDbType.NVarChar).Value = TXT_NOMBRE2_CLIENTE.Text;
                    comando.Parameters.Add("@Apellido1", SqlDbType.NVarChar).Value = TXT_APELLIDO1_CLIENTE.Text;
                    comando.Parameters.Add("@Apellido2", SqlDbType.NVarChar).Value = TXT_APELLIDO2_CLIENTE.Text;

                    comando.Parameters.Add("@FECHA", SqlDbType.Date).Value = TXT_FECHANACI_CLIENTE.Text;
                    comando.Parameters.Add("@GENERO", SqlDbType.Int).Value = Convert.ToUInt32(CBX_GENERO.SelectedValue);
                    comando.Parameters.Add("@DIRECCION", SqlDbType.NVarChar).Value = TXT_DIRECCION_CLIENTE.Text;
                    comando.Parameters.Add("@TELEFONO", SqlDbType.NVarChar).Value = TXT_TELEFONO_CLIENTE.Text;
                    comando.Parameters.Add("@CORREO", SqlDbType.NVarChar).Value = TXT_CORREO_CLIENTE.Text;



                comando.ExecuteNonQuery();



                transaction.Commit();
                MessageBox.Show("CLIENTE ACTUALIZADO");
                Clear();
                LISTARCLIENTES();

                Conexion.Close();
            }

            catch (Exception)

            {
                transaction.Rollback();
                MessageBox.Show("OCURRIO UN FALLO" + e);
                Clear();

                Conexion.Close();
            }
        }

        private void BTN_BUSCAR_CLIENTE_Click(object sender, EventArgs e)
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            //utilizamos el procedimiento alamacenado insertar alumnos
            SqlCommand cmd = new SqlCommand("BUSCAR_CLIENTES", Conexion);
            //especificamos que el comando es un procedimiento almacenado
            cmd.CommandType = CommandType.StoredProcedure;
            //creamos los parametros que usaremos

            cmd.Parameters.Add("@BUSQUEDA", SqlDbType.NVarChar).Value = (TXT_BUSCAR.Text);





            SqlDataAdapter adaptador = new SqlDataAdapter();
            adaptador.SelectCommand = cmd;
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            DGT_CLIENTE.DataSource = tabla;



            Conexion.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BTN_PDF_Click(object sender, EventArgs e)
        {
            }
        }
    }
    
